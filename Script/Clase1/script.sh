#!/bin/bash <---- Interprete

# Inicio de variables comunes

A=2 
B=3

echo "$A + $B"

# Variables inteligentes (con comandos)

RES=$(echo "$A + $B" | bc)

# Condicional de test
# para mas info "man test"

if [[ $RES -gt 5 ]]; then
    echo "mayor que $RES"
elif [[ $RES -eq 5 ]]; then
    echo "el resultado es $RES"
else 
    echo "menor que $RES"
fi

# Condicional for con autosuma de variable
echo "Condicional for con autosuma de variable"
for ((i = 0 ; i <= $RES ; i++)); do
  echo "Counter: $i"
done

# Condicional for con autosuma sin variables

echo "Condicional for con autosuma sin variables"
# desde hasta
for i in {0..10}; do
  echo "Counter: $i"
done

# Condicional for con autosuma sin variables intercalado

echo "Condicional for con autosuma sin variables intercalado"
# desde hasta intercalado
for i in {0..10..2}; do
  echo "Counter: $i"
done

# Condicional for con autosuma con variables

echo "Condicional for con autosuma con variables"
# desde hasta
for i in $( eval echo {0..$RES} ); do
  echo "Counter: $i"
done

# Condicional for con autosuma con variables intercalado

echo "Condicional for con autosuma con variables intercalado"
# desde hasta
for i in $( eval echo {0..$RES..2} ); do
  echo "Counter: $i"
done

# Condicional for con ls (comandos)

#echo "Condicional for con ls (comandos)"
#
#for i in $(ls -a /root); do
#    if [[ $i = "kustomize" || $i = ".minikube"  ]]; then
#        echo "es $i"
#    elif [[ $i = ".putty" ]]; then
#        break
#    fi
#done
#
#
#read -p "Username: " USERNAME
#echo "Hola $USERNAME"
#
#read -sp "Password: " PASSWORD
#echo ""
#echo "Esta es su password: $PASSWORD" 
#
#echo "Conexion con ssh"
#echo "sshpass -p $PASSWORD $USERNAME@server"
#
#echo "Conexion con scp"
#echo "sshpass -p $PASSWORD scp pipi.txt $USERNAME@server:/root/pipi.txt"

read -p "Username: " USERNAME
read -sp "Password: " PASSWORD
echo ""

for i in $(ls -a /root); do
    if [[ $i = "kustomize" || $i = ".minikube"  ]]; then
        echo "sshpass -p $PASSWORD scp $i $USERNAME@server:/root/$i"
    fi
done
